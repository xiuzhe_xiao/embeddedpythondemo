#include <iostream>
#include "demoClass.hpp"
#include <python3.6/Python.h>
#include "PythonWrapper.hpp"

using namespace std;

demoClass demo;

static PyObject *demo_print(PyObject *self, PyObject *args) {

	if (PyArg_ParseTuple(args, "")) {
		demo.demoPrint();
		return PyBool_FromLong(1);
	}
}

static struct PyMethodDef demoMethods[] = {
		{"demoPrint",  demo_print, METH_VARARGS, "demo print"},
		{NULL, NULL, 0, NULL}
};

static struct PyModuleDef demoPythonDef = {
		PyModuleDef_HEAD_INIT, "demo", NULL, -1, demoMethods,
		NULL, NULL, NULL, NULL
};

static PyObject *PyInit_demo(void) {
	return PyModule_Create(&demoPythonDef);
}

int main(int argc, char *argv[])
{

	setenv("PYTHONPATH", ".", 1);

	PyImport_AppendInittab("demo", &PyInit_demo);
	Py_Initialize();
	PythonWrapper pythonWrapper;
	PyObject *pModule = pythonWrapper.import_python_module("config");
	PyObject *pConfig = pythonWrapper.instantiate_python_class("Config", pModule, NULL);
	pythonWrapper.call_method_on_object("print_from_c", pConfig, NULL);
	pythonWrapper.call_method_on_object("print_from_python", pConfig, NULL);
	Py_Finalize();

	return 0;
}
